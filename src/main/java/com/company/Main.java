package com.company;


import org.javaswift.joss.client.factory.AccountConfig;
import org.javaswift.joss.client.factory.AccountFactory;
import org.javaswift.joss.model.Account;

public class Main {

    //region properties
    private String username="user";
    private String password="pass";
    private String authUrl="http://";
    private String tenantId="tenantId";
    private String tenantName="tenantName";
    //

    public static void main(String[] args) {
	    Main main = new Main();
        main.Init();
    }

    private void Init()
    {
        AccountConfig config = new AccountConfig();
        config.setUsername(username);
        config.setPassword(password);
        config.setAuthUrl(authUrl);
        config.setTenantName(tenantName);

        Account account = new AccountFactory(config).createAccount();

    }
}
